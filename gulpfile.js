//
// How to launch it:
// -----------------------------------------------------------------------------
// Reload when save file: $ sudo gulp auto-reload --task default
// Launch for production: $ sudo gulp prod
// Launch for development: $ sudo gulp
//

// Include gulp
var gulp = require('gulp');
var argv = require('yargs').argv; // for args parsing
var spawn = require('child_process').spawn;

// Include Our Plugins
var clean = require('gulp-clean');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cssmin = require('gulp-csso');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var postcss    = require('gulp-postcss');
var foreach = require('gulp-foreach');

var injectPartials = require('gulp-inject-partials');

// Lint Task
gulp.task('lint', function() {
    return gulp.src([
        '!assets/js/jquery-3.2.1.min.js',
        'assets/js/*.js'
        ])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Clean Task
gulp.task('clean', function() {
    return gulp.src('dist/**/*', {read: false})
        .pipe(clean());
});


// Compile Our Sass
gulp.task('sass-dev', function() {
    return gulp.src([
        'assets/scss/main.scss'
    ])
    .pipe(foreach(function(stream, file){
          return stream
            //.pipe(sourcemaps.init())
            .pipe(sass())
            .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
            .pipe(rename({suffix: '.min'}))
            //.pipe(sourcemaps.write('./maps'))
            .pipe(gulp.dest('dist'));
    }));
});

gulp.task('sass-prod', function() {
    return gulp.src([
        'assets/scss/main.scss'
    ])
    .pipe(foreach(function(stream, file){
          return stream
            //.pipe(sourcemaps.init())
            .pipe(sass())
            .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
            .pipe(cssmin())
            .pipe(rename({suffix: '.min'}))
            //.pipe(sourcemaps.write('./maps'))
            .pipe(gulp.dest('dist'));
    }));
});


// Concatenate & Minify JS
gulp.task('js-dev', function() {
    return gulp.src([
        //'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery/dist/jquery.cookie/jquery.cookie.js',
        'node_modules/dotdotdot/src/js/jquery.dotdotdot.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'assets/js/script.js'
        ])
        //.pipe(sourcemaps.init())
        .pipe(concat('script.js'))
        .pipe(rename('script.min.js'))
        //.pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('dist'));
});

gulp.task('js-prod', function() {
    return gulp.src([
        'node_modules/jquery/dist/jquery.cookie/jquery.cookie.js',
        'node_modules/dotdotdot/src/js/jquery.dotdotdot.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'assets/js/script.js'
        ])
        .pipe(concat('script.js'))
        .pipe(rename('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});


////////////////////////////////////////////////////////////////////////////////

// Watch Files For Changes
gulp.task('watch-css', function() {
    gulp.watch(['assets/scss/*.scss'], ['log-css', 'default']);
});

gulp.task('watch-js', function() {
    gulp.watch(['assets/js/*.js'], ['log-js', 'default']);
});

gulp.task('log-css', function() {
    console.log('CSS has been changed');
});

gulp.task('log-js', function() {
    console.log('JS has been changed');
});

////////////////////////////////////////////////////////////////////////////////

gulp.task('html', function () {
  return gulp.src('pages/*')
           .pipe(injectPartials())
           .pipe(gulp.dest('dist/html'));
});

// Default Task
gulp.task('default', ['clean', 'lint', 'sass-dev', 'js-dev', 'watch-css', 'watch-js', 'html']);

// Prod Tasks
gulp.task('prod', ['clean', 'lint', 'sass-prod', 'js-prod', 'html']);
