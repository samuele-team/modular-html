# MODULAR HTML #

Quick and simple solution to create modular HTML page without using a server side language (such as PHP) and setup a server environment.

### What is this repository for? ###

* Generate modular HTML pages 
* Version 1.0.0

### How do I get set up? ###

* Fork the repository to your project directory
* Make sure Node.JS NPM (https://www.npmjs.com) is installed on your machine
* From the project direcotry launch $ npm install
* The results will be saved into the /dist/html directory